#!/bin/bash

set -e

exec "/opt/odoo/odoo-bin" "-c" "/etc/odoo.conf"

exit 1