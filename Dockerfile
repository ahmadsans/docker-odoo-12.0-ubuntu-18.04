FROM ubuntu:18.04
LABEL maintainer="Ahmad Sanusi <ahmad@sanusi.id>"

# Generate locale C.UTF-8 for postgres and general locale data
ENV LANG C.UTF-8

RUN set -x; \
        apt-get update \
        && apt-get -y install git curl python3 python3-pip build-essential wget python3-dev python3-wheel libxslt-dev libzip-dev libldap2-dev libsasl2-dev python3-setuptools node-less libpng-tools

RUN wget -c https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.trusty_amd64.deb

RUN set -x;\ 
        dpkg --force-depends -i wkhtmltox_0.12.5-1.trusty_amd64.deb \
        && apt-get -y install -f --no-install-recommends

RUN set -x; \
        apt-get -y install libpq-dev libxml2-dev libxslt-dev \
        && pip3 install -r https://github.com/odoo/odoo/raw/12.0/requirements.txt

RUN set -x; \
        pip3 install psycopg2 \
        && git clone --depth 1 --branch 12.0 https://github.com/odoo/odoo.git /opt/odoo \
        && adduser --system --quiet --shell=/bin/bash --home=/opt/odoo --gecos 'ODOO' --group odoo \
        && adduser odoo sudo \
        && chown -R odoo:odoo /opt/odoo

# Copy entrypoint script and Odoo configuration file
RUN pip3 install num2words xlwt simplejson
COPY ./entrypoint.sh /
COPY ./odoo.conf /etc/
RUN chown odoo /etc/odoo.conf

# Mount /var/lib/odoo to allow restoring filestore and /mnt/extra-addons for users addons
RUN mkdir -p /opt/extra-addons \
        && mkdir -p /var/log/odoo \
        && mkdir -p /var/lib/odoo \
        && chown -R odoo:odoo /opt/extra-addons \
        && chown -R odoo:odoo /var/lib/odoo \
        && chown -R odoo:odoo /var/log/odoo
VOLUME ["/opt/extra-addons", "/var/log/odoo", "/var/lib/odoo"]

# Expose Odoo services
EXPOSE 8069 8071

# Set the default config file
ENV ODOO_RC /etc/odoo.conf

# Set default user when running the container
USER odoo

ENTRYPOINT ["/entrypoint.sh"]
# CMD ["/opt/odoo/openerp-server -c /etc/odoo.conf"]